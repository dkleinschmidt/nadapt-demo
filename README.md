# JS Adapt Demo w/ Node back end

## Basic set up

1. Make sure node, npm, and couchdb are installed.
2. Clone this repository.
3. Install dependencies (including `npm`, if necessary):

        cd ~/path/to/cloned/repo/
        npm install

4. Run the blank experiment:

        npm start

5. Go to [http://localhost:14607/experiment] in your browser, and
   type in some stuff.
6. Profit.

## How it works

### Client side

There are a couple of options for how to do things on the client side.  For receiving an assignment, you can do a MTurk style `GET /experiment?workerId=`, which will return a templated version of the `views/experiment.html` file.  Replace this with your own HTML/SWIG file.  The example experiment just plugs the `assignment` JS object from `AssignmentManager.getAssignment` into a `<script>` block.  In this case, that block is loaded after the others, which isn't an issue because all the acutal experiment logic only runs once the DOM is ready (see for example, public/expt_empty.js).  For the demo "experiment", the logic is very simple, and just plugs the assignment object into the Experiment object which looks for a `this.assignment.assignmentId` when its `init()` method is called, and then posts back whatever you write in the comment box.

You could also write your own routes that do whatever you'd like.  One other obvious option would be to have a client-side AJAX `POST /get_assignment` or something, instead of putting the assignment js in a template.

For receiving data, there's a route for `POST /submit_results`, which just takes the body of the posted form, pulls out the `assignmentId` field, finds the corresponding database record, and writes the whole form js object as `data`.

### Experiment configuration

The app will read overall app configuration from the `config.json` file.  Options to confgure are:

* `verbose` 
* `port` that the app will listen on.
* `experiment` to run (needs to be listed in `experiment_list.json`, see below for format).
* `AssignmentManager`: the config object passed to the AssignmentManager, which looks for:
    * `verbose` (default: `false`).
    * `no_unclaimed_assignment_strategy` (default: `"error"`; see below).
    * `db`: database connection configuration object, including
        * `db_name`
        * `db_url`
        * `db_port`

The experiment list is loaded from `experiment_list.json` file, which has the format

    {
        "experiment_name": [
            { "condition": "condition1",
              "param1": "value"
            },
            { "condition": "condition2",
              "param1": "another value"
            },
            ...
        ],
        "other_experiment_name": [
            ...
        ]
    }


### `AssignmentManager`

This is the node module that interfaces with the database and manages individual assignments.  It has methods to add assignments, get an assignment for a particular worker/subject, and finish an assignment.  Use it by

    :::javascript
    var config = { db: { db_name: "testing" } };
    var am = require("./AssignmentManager")(config);

This will cause the database named `"testing"` to be created if it doesn't exist already.  The methods are all asynchronous: they take a callback function that will be called on completion with the results as the second argument, or on an error, passed as the first argument.  Internally, the interface with the database is deinfed in `experiment_db.js`.

* `AssignmentManager.addAssignments( assignmentList, callback )`

    Add the given assignments to the manager.  Each assignment to be added can be specified as an object with the conditions parameters for that assignment, or as an object with the fields `condition` (an object with condition parameters) and `count`, the number of assignments to add with those parameters.  Before adding, each assignment will have the `unclaimed` field set to `true`, and the `assignmentid` field set to `null`.

    `callback` should be a node-style callback with the signature `function(err, res)`.

* `AssignmentManager.getAssignment( assignmentInfo, callback )`

    Asynchronously get an assignment for the specified assignment metadata (e.g., worker ID, MTurk assignment ID).  Internally, this pulls an assignment object (created by `addAssignment`) and merges it with assignmentInfo, overwriting any fields (like `assignmentid`) with the provided values and setting the `unclaimed` field to `false`.  Finally, the updated record is written to the database, and returned via the callback.

    In case there's no unclaimed assignments available, an error will be thrown.  If you'd rather have the manager create a new one on the fly based on the conditions associated with the assignments previously added, use the `no_unclaimed_assignment_strategy` config option:

        :::javascript
        AssignmentManager({ no_unclaimed_assignment_strategy: "random" });

    Available options are
    
    * `"random"`: pick a previously added condition at random.
    * `"error"` (default): throw an error (`"NoUnclaimedAssignment"`).

