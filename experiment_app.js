var http = require('http'),
    express = require('express'),
    ecstatic = require('ecstatic'),
    fs = require('fs'),
    cons = require('consolidate'),
    _ = require('underscore'),
    AssignmentManager = require('./AssignmentManager.js');

// list of experiments and associated conditions
var experiment_list = require('./experiment_list.json');
// configuration of app and various components
var expt_config_default = {
    port: 8080,
    verbose: false
};
var expt_config = _(expt_config_default).extend(require('./config.json'));
var experiment_name = expt_config.experiment;
var experiment_conditions = experiment_list[experiment_name].conditions;

var app = module.exports = express();
app.server = http.createServer(app);

// AssignmentManager configuration

app.am = AssignmentManager(expt_config.AssignmentManager);
// add conditions
if (expt_config.init_conditions) {
    app.am.on("ready", function() {
        app.am.addConditions(experiment_conditions);
    });
}

// Express configuration

app.configure(function() {
    // parse bodies of responses for easy access to form entries
    app.use(express.bodyParser());
    // default to ecstatic for everything else (static files)
    app.use(ecstatic({ root: __dirname + '/public' }));
    // rendering views (templates)
    app.engine('html', cons.swig);
    app.set('view engine', 'html');
    app.set('views', __dirname + '/views');
});

// save posted data to DB, and set status to "submitted"
app.post('/submit_results', function(req, res) {
    var status = "submitted";
    var data = req.body;
    if (expt_config.verbose) console.log('Received submitted results:');
    if (expt_config.verbose) console.log(data);
    app.am.finishAssignment(data.assignmentId, status, data, console.log);
    res.send(200, data);
});

app.get('/index', function(req, res) {
    res.render('index', {conditions: experiment_conditions});
});

app.get('/new_subject', function(req, res) {
    res.render('new_subject');
});

// route to handle assignment of conditions
app.get('/experiment', function(req, res) {
    // pull out assignment info from request
    var info = req.query;
    if (expt_config.verbose) console.log('Experiment requested with query', info);
    // render template (eventually), and return
    function sendAssignment(err, assignment) {
        if (err) {
            // handle error
            res.status(500).send(err);
        } else {
            // res.status(200).send(assignment);
            if (expt_config.verbose) console.log('...sending assignment', assignment);
            res.render('experiment',
                       {assignment: JSON.stringify(assignment),
                        name: experiment_name});
        }
    }
    // get an assignment from the AssignmentManager, and send it back
    app.am.getAssignment(info, sendAssignment);
});

// route for specified condition
app.get('/experiment/:condition', function(req, res, next) {
    var condition = req.params.condition;
    var options = _.pluck(experiment_conditions, "condition");
    if (_.contains(_.pluck(experiment_conditions, "condition"), condition)) {
        //
        res.redirect('expt_vroomen_replication.html?condition=' + condition);
    } else {
        // not a real condition
        res.send(404, "No such condition: " + condition + "(Options are" + options + ")");
    }
});


app.server.listen(expt_config.port);

console.log('Listening on port', expt_config.port);
