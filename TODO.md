# TODO

* clean up code
    * whitespace
    * " -> '

* populate claimed/unclaimed assignments from db when app starts

# Minimum functionality:

* Serve up an assignment on demand
* Allow existing experiment code to get dumped into a subdirectory; server just re-directs based on a config file.
* ...checking whether worker has done a previous assignment

# Back-end

## Database

* NoSQL (MongoDB? CouchDB) or something else (Posgres/SQLite).
* How to structure data.
    * One DB per experiment? Yes.
    * Individual trials, or aggregate data? Depends on the database.  Mongo or couch or other NoSQL might work better with a really dumb system with mulitple things stored side-by-side (assignment record, individual trials, etc.).  SQL style would work better with everything nested.
* Integrate with HIT-posting code, too?

### Basic first pass DB/AssignmentGetter ###

* Just store a record of each worker/HIT/assignment ID combo (plus status?)
* When asked for an assignment, check for existing record with worker ID
    * If found (started or submitted), return "sorry" page.
    * If not, create a record (and pick conditions??)
* When HIT started, mark as started 
* When HIT complete, mark assignment as submitted (why?)
* When HIT returned, ping times out, or window unloads, mark assignment as returned (or just delete record).

What is the point of all this marking?  We'd know when people accept/start/submit, for one.  But is that important?  If we're storing trial-by-trial data then it becomes kind of redundant.

The condition assignment stuff is pretty much independent of the database.  Just need to have some variable (or field of `app`) that keeps track of how many assignments are needed in each condition, updating as necessary.  Is there any danger with doing this updating using a counter variable?  I don't think so, and I'm not sure how else a server would handle, e.g., a "users online" count.

The tricky thing is how to handle people who have tech difficulties etc.  The simplest way is to have an `onUnload` or whatever handler in the client code, but that'll only work if someone closes the window and things are still working okay.  The _right_ thing to do is have some kind of heartbeat that's sent back to the server every couple seconds.  Then the server keeps track of the last time it was polled with each AssignmentID.

### Configuration of assignments etc. ###

Let's walk through how a batch of subjects would be run.

1. Load config file with conditions + numbers, parse into data structure with number of assignments needed per condition.
2. Create one HIT with the right (total) number of assignments requested.
3. When an Assignment is requested by MTurk (GET request to the `/experiment` route), poll DB, choose condition, update counter, and redirect to the experiment HTML with the right URL parameters
4. Subjects complete or return HITs.  On return/timeout, check their condition in the database and update counter.
4. ...That's it?

### How to factor things ###

* app 
* AssignmentManager
    * Provide info on which conditions and how many assignments of each
    * hold onto:
        * un-assigned assignments (condition info) 
        * assigned assignments (dict of assignment iD => condition info + worker ID etc.)
    * methods for:
        * start/create assignment, given worker id etc.
        * release assignment given assignment id
        * finish assignment: 

## Templates

* "Done this/similar HIT before"
* Template for experiment stuff (or use an AJAX call in front-end?).  Very easy to just add a `<script>` tag with the required info.

# Front-end

* AJAX call for each trial, and POST at finish?
* replace URL parameters with AJAX calls, or with templated info?

# Experiment logic

* Could move to back-end, as a node module
* [Browserify all the js-adapt stuff](http://ampersandjs.com/learn/npm-browserify-and-modules), and turn it into a module.  This is gonna be a ton of trouble though, and will probably also require moving to Backbone.js or something at the same time.

# Lab-based adaptation

Just needs to provide a screen for entering subject info, and store results in DB.  Don't need to worry about checking people

## Pseudocode:

### Initialization

* Store assignments needed in database

        AssignmentManager.addAssignments([ {condition: {...}, count: <n>},
                                           {condition: {...}, count: <n>}, ... ]);

* Use a node script to read a json or yaml or something file that has the parameters of each condition and how many subjects are needed, and then call `addAssignments`.
* (or make some kind of dashboard, but that would be a ton of trouble)

* GET to /lab_start serves up form with subject info prompts (probably just name?)
* subject info POSTed to /experiment or something and
    * uses AssignmentManager to get assignment info (condition) from db (and associate that assignment with this subject
        * get all unclaimed assignments
        * randomly select one, with provided info, and then write
    * app redirects to the requested page (with proper URL parameters for workerID, assignmentID, etc.)
* Experiment is over, POST to /submit_results
    * uses AssignmentManager.finishAssignment to write form data JSON to db on assignment
