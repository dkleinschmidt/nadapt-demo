var cradle = require("cradle"),
    _ = require("underscore"),
    events = require("events");


module.exports = function(config_in) {
    var config_defaults = {
        db_name: 'test',
        db_url: 'http://127.0.0.1',
        db_port: 5984,
        verbose: false
    };

    var config = _.extend(_.clone(config_defaults), config_in);

    if (config.verbose) {
        console.log("Setting up database connection:")
        console.log("  Default configuration:", config_defaults, "\n",
                    " Provided configuration:", config_in, "\n",
                    " Combined configuration:", config);
    }

    var c = new(cradle.Connection);
    var db = c.database(config.db_name);
    _(db).extend(new events.EventEmitter);

    // Set up design documents, and any other configuration that's needed.
    var init_counter = 0;
    db.init = function() {
        init_counter++;
        if (config.verbose) console.log("db.init called (", init_counter, ")");
        // add design docs
        // view by worker id:
        var init_worker_design = function(callback) {
            db.save('_design/workers', {
                all: {
                    // get all records with a workerId associated
                    map: function(doc) {
                        if (doc.workerId) emit(doc.workerId, doc);
                    }
                }
            }, callback);
        };

        // view by assignment id:
        var init_assgn_design = function(callback) {
            db.save('_design/assignments', {
                all: {
                    // get all assignments (records with an assignment id)
                    map: function(doc) {
                        if (typeof(doc.assignmentId) !== "undefined") emit(doc.assignmentId, doc);
                    }
                },
                unclaimed: {
                    // get all unclaimed assignments
                    map: function(doc) {
                        if (typeof(doc.assignmentId) !== "undefined" && doc.unclaimed) {
                            emit(doc.assignmentId, doc);
                        }
                    }
                },
                claimed: {
                    // get all CLAIMED assignments
                    map: function(doc) {
                        if (typeof(doc.assignmentId) !== "undefined" && ! doc.unclaimed) {
                            emit(doc.assignmentId, doc);
                        }
                    }
                }
            }, callback);
        }

        if (config.verbose) console.log("Trying to initialize workers design");
        init_worker_design(function(err, res) {
            if (err) {
                db.emit("error", err);
            } else {
                if (config.verbose) console.log("Trying to initialize assignments design");
                init_assgn_design(function(err, res) {
                    if (err) {
                        if (config.verbose) console.log("WTH some kind of error initializing assignment docs?");
                        db.emit("error", err);
                    } else {
                        db.emit("dbInitialized");
                        db.emit("ready");
                    }
                });
            }
        });
    }

    // if database doesn't exist, then create and initialize it.
    db.exists(function(err, exist) {
        if (err) {
            // TODO: handle error?
            throw(Error(err));
        } else if (exist) {
            if (config.verbose) console.log('Database ' + config.db_name + ' exists, good to go.');
            db.emit("ready");
        } else {
            // need to create DB and initialize
            if (config.verbose) console.log('Database ' + config.db_name + ' does not exist; creating...');

            // create database, emitted "created" event on success
            db.create(function(err, res) {
                if (err) {
                    if (config.verbose) console.log("Error creating database: ", err);
                    db.emit("error", err, null);
                } else {
                    if (config.verbose) console.log("Database created")
                    db.init();
                    // TODO: remove this event (need to fix in tests/assignment_manager.js)
                    db.emit("created", res);
                }
            });

        }
    });

    return(db);
}
