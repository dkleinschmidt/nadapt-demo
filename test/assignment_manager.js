var _ = require("underscore"),
    events = require("events");


function spin_up(callback) {
    var config = {
        verbose: true,
        db: {
            db_name: 'test' + Math.round(Math.random() * 1000000000)
        }
    };

    var am = require("../AssignmentManager.js")(config);

    console.log('Database name: ', am.db.name);
    am.db.on("created", function(res) {
        console.log("Database created event caught: ", res);
        callback(res);
    });

    return(am);
}

function reset(am) {
    var db = am.db
    am.clearUnclaimedAssignments();
    // remove all documents
    db.all(function(err, res) {
        if (res) {
            console.log("Trying to clear", res.length, "documents");
            var n_to_remove = res.length;
            var n_removed = 0;
            _(res).each(function(doc) {
                db.remove(doc.id, doc.value._rev, 
                          function(err, res) {
                              if (err) {
                                  console.log(err);
                                  runner.emit("error", err, "removing doc");
                              } else {
                                  n_removed = n_removed + 1;
                                  console.log("Removed", n_removed, "out of", n_to_remove);
                                  if (n_removed >= n_to_remove) {
                                      db.emit("cleared");
                                      db.init();
                                  }
                              }
                          });
            });
        }
    });
}
            

function check_exists(db, callback) {
    db.exists(callback || console.log);
}

// function clear_view(db, view_name) {
//     db.view(view_name, function(err, res) {
//         if (err) {
//             console.log("Error viewing assignments: ", err);
//             db.emit("error", err, null);
//         } else {
//             console.log("Trying to clear", res.length, "assignments");
//             var n_to_remove = res.length;
//             var n_removed = 0;
//             _(res).each(function(doc) {
//                 db.remove(doc.value._id, doc.value._rev,
//                           function(err, res) {
//                               if (err) {
//                                   console.log(err);
//                                   db.emit("error", err);
//                               } else {
//                                   n_removed = n_removed + 1;
//                                   console.log("Removed", n_removed, "out of", n_to_remove);
//                                   if (n_removed >= n_to_remove) {
//                                       db.emit("cleared");
//                                   }
//                               }
//                           });
//             });
//         }
//     });
// }

// function clear_assignments_and_workers(db) {
//     clear_view(db, "assignments/all");
//     db.once("cleared", function() {
//         clear_view(db, "workers/all");
//     });
// }

// asynchronous teardown function
function tear_down_db(db) {
    console.log("Tearing down database", db.name);
    check_exists(db, function(err, res) {
        if (res) db.destroy(function(err, res) {
            if (err) {
                runner.emit("error", err, "destroying db");
            } else {
                db.emit("destroyed", null, res);
            }
        });
        else console.log(err, res);
    });
}

// helper fun to get views
var show_views = function(views, callback) {
    console.log("All: ")
    console.dir(_(views.all).pluck('value'));
    console.log("Unclaimed: ")
    console.dir(_(views.unclaimed).pluck('value'));
    callback();
};

var retrieve_views = function(am, views, callback) {
    var db = am.db
    db.view("assignments/all", function(err, res) {
        if (err) runner.emit("error", err, "view assignments/all");
        views.all = res
        db.view("assignments/unclaimed", function(err, res) {
            if (err) runner.emit("error", err, "view assignments/unclaimed");
            views.unclaimed = res;
            callback();
        });
    });
};

// test specific functions:

// addAssignment
function test_addAssignments() {
    var assignments = [ {condition: {a: 1, b: 2}, count: 3},
                        {condition: {a: 2, b: 3}, count: 2},
                        {a: 2, b: 2} ];
    var views = {};

    var am = runner.am;
    var db = am.db;
    
    am.addAssignments(assignments, function(err, res) {
        console.log("Assignments added: ", res);
        retrieve_views(am, views, function() {
            show_views(views, function() { runner.emit("next"); });
        });
    });
}

// randomUnclaimedAssignment
function test_randomUnclaimedAssignment() {
    var assignments = [ {condition: {a: 1, b: 2}, count: 3},
                        {condition: {a: 2, b: 3}, count: 2},
                        {a: 2, b: 2} ];

    var am = runner.am;
    var db = am.db;
    
    var rand_assgn = [];
    var n_rand_assgn = 6;

    var receive_assignment = function(err, res) {
        if (err) {
            runner.emit("error", err, "receiving assignment");
        } else if (res) {
            rand_assgn.push(res);
            console.log("Received ", rand_assgn.length, " out of ", n_rand_assgn);
            if (rand_assgn.length >= n_rand_assgn) {
                console.log(rand_assgn);
                runner.emit("next");
            }
        } else {
            throw(new Error("No result yielded by randomUnclaimedAssignment"));
        }
    };

    var views = {};
    
    am.addAssignments(assignments, function(err, res) {
        //console.log("Cached unclaimed assignments: ", am.cachedUnclaimedAssignments());
        //retrieve_views(am, views, function() {
            for (var i=0; i<n_rand_assgn; i++) {
                am.randomUnclaimedAssignment(receive_assignment);
            }
        //});
    });
    
}

// getAssignment
// 
// This is where the magic happens: retrieves an unclaimed assignment, saves it
// along with assignment metadata provided, and returns json object.
//
// A couple of cases to test:
// 
// 1. Everything okay: unclaimed assignments exist.
// 2. No assignments there.


    

function test_getAssignment() {
    var am = runner.am;
    var db = am.db;

    var views = {};
    
    am.addAssignments([{a: 1, b: 2}], function(err, res) {
        console.log("Added an assignment");
        am.getAssignment( {workerId: 'okay', x: 'y'}, function(err, res) {
            if (err) runner.emit("error", err, "getAssignment");
            retrieve_views(am, views, function() {
                show_views(views, function() { runner.emit("next"); });
            });
        });
    });
}

function test_getAssignment_oldWorker() {
    var am = runner.am;
    var db = am.db;

    var views = {};

    db.save({workerId: 'notokay'}, function(err, res) {
        console.log('Saved ', res);
    });
    
    am.addAssignments([{a: 1, b: 2}], function(err, res) {
        console.log("Added an assignment");
        am.getAssignment( {workerId: 'notokay', x: 'y'}, function(err, res) {
            if (err && err.name == 'OldWorker') {
                console.log("Old worker detected successfully");
                runner.emit("next");
            } else if (err) {
                runner.emit("error", err, "in getAssignment (should be OldWorker)");
            } else {
                runner.emit("error", res, "should throw OldWorker error");
            }
        });
    });
}


function test_getAssignment_noAssignment() {
    var am = runner.am;

    am.getAssignment( {workerId: 'notokay', x: 'y'}, function(err, res) {
        if (err && err.name == 'NoUnclaimedAssignment') {
            console.log("No unclaimed assignment error detected successfully");
            runner.emit("next");
        } else if (err) {
            runner.emit("error", err, "in getAssignment (should be NoUnclaimedAssignment)");
        } else {
            runner.emit("error", res, "should throw NoUnclaimedAssignment error");
        }
    });
}

function test_getAssignment_lots() {
    var am = runner.am;

    var n_to_add = 100;
    var n_seen = 0;

    var n_errs = 0;
    var all_assignments = [];

    var request_delay = 0; // ms between requests
    
    am.addAssignments([{ condition: {a: 1}, count: n_to_add }], function(err, res) {
        if (err) runner.emit("error", err);
        for (var i = 0; i < n_to_add; i++) {
            (function(ii) {
                setTimeout(function() { 
                    am.getAssignment({workerId: ''+ii, assignmentRequestTime: Date.now()}, function(err, res) {
                        n_seen++;
                        console.log("Try", n_seen, "out of", n_to_add);
                        if (err) {
                            console.log("Error!", err);
                            n_errs++;
                            // runner.emit("error", err);
                        } else {
                            console.log("Success!", res);
                            res.assignmentDeliveryTime = Date.now();
                            res.assignmentDeliveryDelay = res.assignmentDeliveryTime - res.assignmentRequestTime;
                            all_assignments.push(res);
                        }

                        if (n_seen >= n_to_add) {
                            if (n_errs) {
                                runner.emit("error", n_errs + " during getAssignment test");
                            }
                            var retries = _(all_assignments).pluck("retriesRequired");
                            // console.log(_(all_assignments).pluck("assignmentDeliveryDelay"));
                            var total_retries = _(retries).reduce(function(x,y) {return (x?x:0)+y;});
                            console.log("Done! Required",
                                        total_retries ? total_retries : 0,
                                        "retries for",
                                        n_to_add,
                                        "simultaneous assignment requests");
                            var views = {};
                            retrieve_views(am, views, function() {
                                console.log("There are", views.all.length, "total assignments in the db,",
                                            views.unclaimed.length, "of which are unclaimed");
                                runner.emit("next");
                            });
                        }
                        
                    });
                }, request_delay*ii);
            })(i);
        }
    });
}


var runner = new events.EventEmitter;
runner.tests = [];
runner.on("next", function() {
    var am = runner.am
    var db = am.db;
    if (runner.tests.length) {
        reset(am);
        db.once("dbInitialized", function() {
            console.log("\n\nRunning next test...");
            f = runner.tests.shift();
            f();
        });
    } else {
        console.log("No more tests to run");
        runner.emit("finished");
    }
});

runner.on("error", function(err, msg) {
    console.log(err, msg);
    tear_down_db(runner.am.db);
    throw(new Error(err));
});

runner.on("finished", function() {
    tear_down_db(runner.am.db);
})

// basic functionality: adding and getting a random unclaimed assignment
runner.tests.push(test_addAssignments);
runner.tests.push(test_randomUnclaimedAssignment);

// // getAssignment
runner.tests.push(test_getAssignment);
runner.tests.push(test_getAssignment_oldWorker);
runner.tests.push(test_getAssignment_noAssignment);
runner.tests.push(test_getAssignment_lots);

runner.am = spin_up(function() {runner.emit("next");});
