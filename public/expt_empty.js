// global variable to be created by template or otherwise.
var assignment;

$(document).ready(
    function() {
        
        // create an experiment object with the necessary RSRB metadata
        e = new Experiment(
            {
                rsrbProtocolNumber: '000000000000',
                rsrbConsentFormURL: 'http://www.hlp.rochester.edu/consent/RSRB45955_Consent_2014-02-10.pdf',
                rsrbDoDemographicSurvey: false,
                submitTarget: '/submit_results',
                assignment: assignment
            }
        );

        e.addBlock(new InstructionsBlock("<h3>This is {0}: {1}!</h3>".format(expt_name, assignment.condition.condition)));

        e.init();

        e.nextBlock();
    }
);
