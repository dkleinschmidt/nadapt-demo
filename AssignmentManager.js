var _ = require("Underscore"),
    events = require("events");



module.exports = function(config) {
    
    var config_defaults = {
        verbose: false,
        no_unclaimed_assignment_strategy: "error"
    };
    
    var config = _.extend(_.clone(config_defaults), config);
    if (config.verbose && typeof(config.db.verbose) === 'undefined') config.db.verbose = config.verbose;
    
    var db = require('./experiment_db.js')(config.db);
    
    // keep track of the unique conditions that have been added via addAssignments
    var uniqueConditions = [];
    function addToUniqueConditions(cond) {
        if (! _(uniqueConditions).contains(cond)) {
            uniqueConditions.push(cond);
        }
    }

    // add conditions without adding any assignments to the db
    function addConditions(conditionsList) {
        _(conditionsList).each(addToUniqueConditions);
        if (config.verbose) console.log('Added conditions.  Now have:\n', uniqueConditions, '\n');
    }

    // local cache of unclaimed assignments
    var unclaimedAssignments = [];

    // clear the cache of unclaimed assignments
    var clearUnclaimedAssignments = function() { unclaimedAssignments = [] };
    // return a shallow copy of the list of unclaimed assignments: 
    var cachedUnclaimedAssignments = function() { return _.clone(unclaimedAssignments); };

    // local cache of claimed assignments: mapping assignmentId to database id
    var claimedAssignments = {};

    // check db for claimed and unclaimed assignments and add them to the lists
    function initializeAssignmentCaches(callback) {
        claimedAssignments = {};
        unclaimedAssignments = [];
        db.view("assignments/all", function(err, docs) {
            if (err) {
                callback(err);
            } else if (docs.length) {
                var n_claimed = 0;
                var n_unclaimed = 0;
                var n_other = 0;
                _(docs).each(function(doc) {
                    if (doc.value.unclaimed === false) {
                        claimedAssignments[doc.value.assignmentId] = doc.value._id;
                        n_claimed++;
                    } else if (doc.value.unclaimed) {
                        unclaimedAssignments.push(doc.value);
                        n_unclaimed++;
                    } else {
                        n_other++;
                    }
                });
                console.log("Startup: found", docs.length, "assignments in db",
                            n_claimed, "claimed,", n_unclaimed, "unclaimed, and",
                            n_other, "other (neither claimed nor unclaimed)");
                console.log("Claimed: ", claimedAssignments);
                console.log("Unclaimed: ", unclaimedAssignments);
                callback();
            } else {
                console.log("Startup: no assignments found in db.");
                callback();
            }
        });
    }
    
    // populate the database with assignments, with a { unclaimed: true } marker.
    // and assignmentId set to null
    //
    // use the "assignments/unclaimed" view to get the list of unclaimed assignments
    var addAssignments = function(assignmentList, callback) {
        // expand list of {condition: {...}, count: n} to list of all conditions
        var conditions = [];
        _(assignmentList).each(function(assignment) {
            var cond;
            if (assignment.count && assignment.condition) {
                // it's an object with count and condition properties.  add the
                // condition <count> times
                _(assignment.count).times(function() { conditions.push(assignment.condition) });
                cond = assignment.condition;
            } else {
                // it's just a single thing, push to list.
                conditions.push(assignment);
                cond = assignment;
            }
            // add to list of unique conditions
            addToUniqueConditions(cond);
        });

        // add the metadata along with the conditions themselves.
        var assignments = _(conditions).map(function(cond) {
            var assignment = { condition: cond,
                               assignmentId : null,
                               unclaimed : true };
            return(assignment);
        });

        // write to the database and add to cache
        db.save(assignments, function(err, res) {
            if (err) {
                // handle error
                callback(err, null);
            } else {
                // add returned assignments to the list in memory if saved successfully
                _.each(_.zip(res, assignments), function(ra) {
                    // res stores id and rev; doc format stores _id and _rev
                    ra[1]._id = ra[0].id; ra[1]._rev = ra[0].rev
                    unclaimedAssignments.push(ra[1]);
                });
                callback(err, {conditions: conditions, res: res});
            }
        });
    };

    // helper function to retrieve a random unclaimed assignments
    // 
    // (this is a TERRIBLE STRATEGY because it runs the view of the db every
    // single time an assignment is requested)
    var randomUnclaimedAssignment_db = function(callback) {
        db.view('assignments/unclaimed',
                function(err, doc) {
                    if (err) {
                        callback(err, null);
                    } else if (! doc.length) {
                        handleNoUnclaimed(callback);
                    } else {
                        callback(null, _(doc).sample(1)[0].value);
                    }
                });
    };

    var randomUnclaimedAssignment_cached = function(callback) {
        // get an assignment from the list held in memory
        var n_unclaimed = unclaimedAssignments.length;
        if (n_unclaimed > 0) {
            var rand_index = Math.floor(Math.random() * n_unclaimed);
            // Array.splice(idx, 1) removes and returns out a length-1 array
            // starting at index of idx (i.e. Array[idx:(idx+1)])
            var rand_assignment = unclaimedAssignments.splice(rand_index, 1)[0];
            // console.log("Retrieving random cached assignment number", rand_index,
            //             ": ", rand_assignment);
            callback(null, rand_assignment);
        } else {
            handleNoUnclaimed(callback);
        }
    };

    var randomUnclaimedAssignment = randomUnclaimedAssignment_cached;


    // what is the strategy for dealing with there being no unclaimed assignments
    // when one is requested?

    var no_unclaimed_strategies = {
        "random": function(callback) {
            if (uniqueConditions.length) {
                var rand_cond = _(uniqueConditions).sample(1);
                addAssignments(rand_cond, function(err, res) {
                    if (err) callback(err); // error while saving new assignment
                    else randomUnclaimedAssignment(callback);
                });
            } else {
                var err = new Error("Requested random new condition but no conditions added");
                err.name = "NoConditions";
                callback(err);
            }
        },
        "error": function(callback) {
            var err = new Error("No unclaimed assignments");
            err.name = "NoUnclaimedAssignment";
            callback(err, null);
        }
    }
    var handleNoUnclaimed = 
        no_unclaimed_strategies[config.no_unclaimed_assignment_strategy] ||
        no_unclaimed_strategies["error"];

    // claim an assignment, with info in assignmentInfo, and db entry in doc
    var claimAssignment = function(assignmentInfo, doc, callback) {
        // merge provided assignment info properties
        //
        // TODO: sanitize provided properties (e.g. don't clobber doc.value._id,
        // doc.value._rev)
        _(doc).extend(assignmentInfo)
        doc.unclaimed = false;
        // if no assignmentId provided just use the database id
        if (doc.assignmentId === null || typeof(doc.assignmentId) === "undefined") {
            doc.assignmentId = doc._id;
        }
        // check whether assignment with same ID has been claimed
        if (_.has(claimedAssignments, doc.assignmentId)) {
            var err = new Error("Assignment ID already claimed: " + doc.assignmentId);
            err.name = "AssignmentAlreadyClaimed"
            return callback(err);
        } else {
            claimedAssignments[doc.assignmentId] = doc._id;
        }
        // now save and return doc on success
        db.save(doc._id,
                doc._rev,
                doc,
                function(err, res) {
                    if (err) {
                        if (err.error == 'conflict') {
                            if (config.verbose) console.log("Conflict error on getAssignment. Should retry?");
                            if (!assignmentInfo.retriesRequired) {
                                assignmentInfo.retriesRequired = 1;
                            } else {
                                assignmentInfo.retriesRequired++;
                            }
                            getAssignment(assignmentInfo, callback);
                        } else {
                            callback(err, null);
                        }
                    } else {
                        callback(null, doc);
                    }
                });
    }

    // Allocate an assignment to a particular worker ID/hit ID/assignment ID.
    // Assignment info should be provided in an object that includes at least:
    //
    // assignmentInfo = {
    //   assignmentId: 'string',
    //   workerId: 'string'
    // };
    //
    // workerId will be used to look up existing entries in the database to
    // make sure this worker hasn't done this (or similar) hit before.
    var getAssignment = function(assignmentInfo, callback) {
        db.view('workers/all',
                { key: assignmentInfo.workerId },
                function(err, doc) {
                    if (err) {
                        // handle error...
                        callback(err, null);
                    } else if (doc.length) {
                        // found existing record of worker
                        if (config.verbose) console.log('Worker has existing record!');
                        // create and return Error to be handled by callback
                        var err = new Error('Worker has existing record!');
                        err.name = 'OldWorker'
                        callback(err, null);
                    } else {
                        // okay! no previous record of this worker found so
                        // we're good to create a new record
                        if (config.verbose) {
                            console.log('Trying to create a record for');
                            console.dir(assignmentInfo);
                        }
                        // get random unclaimed assignment, and "claim" and return
                        // (via callback())
                        randomUnclaimedAssignment(function(err, doc) {
                            // TODO: handle error of no assignment left, or leave for
                            // top-level to deal with (e.g. retry later)
                            if (err) {
                                callback(err, null)
                            } else {
                                claimAssignment(assignmentInfo, doc, callback);
                            } // end of no-error randomUnclaimedAssignment handler
                        }); // randomUnclaimedAssignment
                    }
                }); // view "workers/all"
    };

    // Update the status of an assignment in the database, optionally with
    // submitted data
    var finishAssignment = function(assignmentId, status, data, callback) {
        db.view('assignments/all',
                { key: assignmentId },
                function(err, docs) {
                    if (err) {
                        // error from the database
                        callback(err, null);
                    } else if (docs.length) {
                        // TODO: check for > 1 doc
                        // found a doc, update it with provided status and data
                        var doc = docs[0];
                        var id = doc.id;
                        var rev = doc.rev;
                        doc.value.status = status;
                        doc.value.data = data;
                        db.save(id, rev, doc.value, callback);
                        //db.merge(id, { status: status, data: data }, callback);
                    } else {
                        // no doc returned
                        callback(Error('No record of assignment found'), null);
                    }
                });
    }

    // Release an assignment, allowing it to be allocated again (if, for
    // instance, the ping times out or the window is closed.
    var releaseAssignment = function(assignmentId, callback) {
        err = new Error("AssignmentManager.releaseAssignment not implemented");
        err.name = "NotImplemented";
        callback(err);
    }

    var self = _({
        clearUnclaimedAssignments: clearUnclaimedAssignments,
        cachedUnclaimedAssignments: cachedUnclaimedAssignments,
        getAssignment: getAssignment,
        finishAssignment: finishAssignment,
        releaseAssignment: releaseAssignment,
        addAssignments: addAssignments,
        addConditions: addConditions,
        randomUnclaimedAssignment: randomUnclaimedAssignment,
        db: db
    }).extend(new events.EventEmitter);

    
    db.on("ready", function() {
        initializeAssignmentCaches(function(err) {
            if (err) self.emit("error", err);
            else self.emit("ready")
        });
    });
    
    return self;
}
